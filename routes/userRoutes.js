const express = require('express')
const userController = require('./../controllers/userController')
const router = express.Router()
//express.router acts as a middleware that helps to navigate between the files

router
    .route('/')
    .get(userController.getAllUsers)
    .post(userController.createUser)

router 
    .route('/:id')
    .get(userController.getUser)
    .patch(userController.updateUser)
    .delete(userController.deleteUser)

module.exports = router