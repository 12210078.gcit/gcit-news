const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name :{
        type: String,
        required: [true, 'Please tell us your name!'],

    },
    email: {
        type: String,
        required:[true, 'Please provide us your email!'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    photo : {
        type:String,
        default: 'default.jpg',
    },
    role: {
        type:String,
        enum:['user', 'admin'],
        default: 'user',
    },
    password : {
        type: String,
        required: [true, 'Please provide a password'],
        minlenght: 8,
        //password wont be INcluded when we get the users
        // select: false
    },
    passwordConfirm: {
        type: String,
        required: [true, 'Please confirm your password'],
        validate: {
            validator: function(el) {
                return el === this.password
            },
            message: 'Passwords do not match',
        }
    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
})
userSchema.pre('save', async function (next) {
    //Only run this function if the password was actually modified
    if(!this.isModified('password'))return next()
    //Hsash the password with the cost of 12 meaning we encrypt it 12 times 
    this.password = await bcrypt.hash(this.password, 12)
    //Delete passwordConfirm field because we don't want to store it in the database 
    this.passwordConfirm = undefined

    //this is used to run the next middleware function
    next()
 })

const User = mongoose.model('User', userSchema)
module.exports = User

