const app = require('./app')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path: './config.env'})


const DB = process.env.DATABASE.replace('PASSWORD',
    process.env.DATABASE_PASSWORD)

    const local_DB = process.env.DATABASE_LOCAL
    mongoose.connect(local_DB).then((con) =>{
        console.log(con.connections)
        console.log('DB connection successful')
    }).catch(error => console.log(error))
    
//mongoose.connect is an inbuilt promise
    
/* Starting the server or port 4001 */
const port = 4001
app.listen(port, () =>{
    console.log(`App running on port ${port} ..`)
})