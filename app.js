const express = require("express")
const userRouter = require('./routes/userRoutes')
const app =  express() //app is a variable that stores all the express libraries
app.use(express.json())

app.use('/api/v1/users', userRouter)
module.exports = app